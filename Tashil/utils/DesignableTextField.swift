//
//  DesignableTextField.swift
//  Tashil
//
//  Created by Ahmed Adm on 30/06/2020.
//  Copyright © 2020 Smm - Ahmed Adm. All rights reserved.
//

import UIKit

class DesignableTextField: UITextField {

     @IBInspectable var stringId: String {
        set(value) {
            self.placeholder = NSLocalizedString(value, comment: "")
            //self.setTitle(NSLocalizedString(value, comment: ""), for: .normal)
        }
        get {
            return ""
        }
     }
     
     
     @IBInspectable var shadowColor: UIColor = UIColor.clear {
         didSet {
             layer.shadowColor = shadowColor.cgColor
         }
     }
     
     @IBInspectable var shadowRadius: CGFloat = 0 {
         didSet {
             layer.shadowRadius = shadowRadius
         }
     }
     
     @IBInspectable var shadowOpacity: CGFloat = 0 {
         didSet {
             layer.shadowOpacity = Float(shadowOpacity)
         }
     }
     
     @IBInspectable var shadowOffsetY: CGFloat = 0 {
         didSet {
             layer.shadowOffset.height = shadowOffsetY
         }
     }
     
     @IBInspectable var cornerRadius: Double {
         get {
             return Double(self.layer.cornerRadius)
         }set {
             self.layer.cornerRadius = CGFloat(newValue)
         }
     }
     
     @IBInspectable var borderWidth: Double {
         get {
             return Double(self.layer.borderWidth)
         }
         set {
             self.layer.borderWidth = CGFloat(newValue)
         }
     }
     
     @IBInspectable var borderColor: UIColor? {
         get {
             return UIColor(cgColor: self.layer.borderColor!)
         }
         set {
             self.layer.borderColor = newValue?.cgColor
         }
     }

}
